# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

MY_PN="pypy"
S="${WORKDIR}/${MY_PN}-${PV}-linux"

DESCRIPTION="A very compliant implementation of the Python language."
HOMEPAGE="http://www.pypy.org"
SRC_URI="http://pypy.org/download/${MY_PN}-${PV}-linux.tar.bz2"

LICENSE="MIT LGPL"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

src_install() {
	dodir /usr/lib/${MY_PN} || die 'dodir failed'
	insinto /usr/lib/${MY_PN}
	cd ${S} || die 'cd failed'
	doins -r ./* || die 'doins failed'
	fperms a+x /usr/lib/${MY_PN}/bin/${MY_PN}
	dosym /usr/lib/${MY_PN}/bin/${MY_PN} /usr/bin/${MY_PN} || die 'dosym failed'
	dodoc LICENSE
}
