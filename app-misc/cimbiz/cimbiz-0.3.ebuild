# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit distutils

DESCRIPTION="Exhange rate program, for Turkish users"
HOMEPAGE="http://www.istihza.com"
SRC_URI="http://www.istihza.com/${PN}/${P}/anakaynak/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-lang/python-2.5"
RDEPEND="${DEPEND}
	dev-python/pygtk"
