# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit subversion eutils python

DESCRIPTION="A Python/GTK+ instant messenger for the Windows Live Messenger network"
HOMEPAGE="http://www.emesene.org"
LICENSE="GPL-2"
KEYWORDS="~x86"
SLOT="0"
IUSE=""

ESVN_REPO_URI="https://emesene.svn.sourceforge.net/svnroot/${PN}/trunk/${PN}"
ESVN_PROJECT="${PN}"

DEPEND=">=dev-lang/python-2.4.3
	>=x11-libs/gtk+-2.10.14
	>=dev-python/pygtk-2.12.1-r2"

src_install() {
	dodir /usr/share/emesene || die 'dodir failed'
	insinto /usr/share/emesene
	doins -r ./* || die 'doins failed'
	exeinto /usr/share/emesene
	doexe emesene || die 'doexe failed'
	dosym /usr/share/emesene/emesene /usr/bin/emesene || die 'dosym failed'
	make_desktop_entry ${PN} "emesene" "/usr/share/emesene/themes/default/icon.png"  
}
pkg_postinst() {
	python_mod_optimize /usr/share/emesene
	ewarn "This is a live ebuild"
	ewarn "That means there are NO promises it will work."
}

pkg_postrm() {
	python_mod_cleanup /usr/share/emesene
}
