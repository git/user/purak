# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit distutils

DESCRIPTION="A simple, fast, small and robust XML parser for Python, based on iksemel"
HOMEPAGE="http://pardus.org"
SRC_URI="http://cekirdek.pardus.org.tr/~bahadir/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-lang/python"
RDEPEND="${DEPEND}"

DOCS="README"
