# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit distutils

DESCRIPTION="HARMAN is a free software that can be used to merge, split and cut PDF documents."
HOMEPAGE="http://www.istihza.com"
SRC_URI="http://www.istihza.com/harman/${P}/anakaynak/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-lang/python-2.5
		sys-devel/gettext"
RDEPEND="${DEPEND}
		dev-python/pyPdf
		dev-python/pygtk"
