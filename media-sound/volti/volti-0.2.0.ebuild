# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit distutils

DESCRIPTION="GTK+ application for controlling audio volume from system tray/notification area"
HOMEPAGE="http://code.google.com/p/volti"
SRC_URI="http://volti.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE="X hal libnotify"

DEPEND="dev-lang/python"

RDEPEND="${DEPEND}
	x11-themes/gnome-icon-theme
	>=dev-python/pygtk-2.16.0
	>=dev-python/pyalsaaudio-0.6
	>=dev-python/dbus-python-0.80.0
	X? ( >=dev-python/python-xlib-0.15_rc1 )
	libnotify? ( x11-libs/libnotify )
	hal? ( sys-apps/hal )"

DOCS="README ChangeLog TODO AUTHORS"
